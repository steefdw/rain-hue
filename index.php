<?php
/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 *
 * Date: 21-6-17
 * Time: 16:52
 */

if(!file_exists(__DIR__ . '/vendor/autoload.php'))
{
    die("\033[31mPlease run 'composer install' first\033[0m".PHP_EOL);
}
require_once __DIR__ . '/vendor/autoload.php';

$app = new \Rain\App($argv);

if(RUNNING_IN_CONSOLE === false)
{
    echo '<pre>';
}
echo $app->welcomeMessage();

$data = \Rain\Rain::fetch($app->arguments);
$out  = new \Rain\Forecast();

$forecast = $out->parseData($data);
echo $forecast->sparkLine().PHP_EOL;

$message = implode(PHP_EOL, $forecast->messages());
echo $message.PHP_EOL;

$lights = new \Rain\Lights($app);
$lights->loop($forecast->alert());
# Rain Hue
Your Raspberry Pi will alert you of coming rain / sun by flashing the Hue lights in your home blue or yellow respectively.
It uses Buienradar forecasts for your location (using the longitude/latitude coordinates you can set in the config file). 

Let your Hue lights flash blue when it is going to rain, let them flash yellow when is going to be sunny in the next 30 minutes.

## Usage

1) Clone this project, cd into it and run:
```bash
composer install
```
2) Rename the `config.example.php` to `config.php` and fill in the values.
3) Check for a new alert to see it is working (see below)
4) Create a cron job to run this command every 5 minutes:
```bash
*/5 * * * * php /var/www/regen/index.php > /dev/null 2>&1
```
  
### check for new alert
cd to your project and run:

```
php index.php
```
![regen-check-alert-cli](https://gitlab.com/steefdw/rain-hue/uploads/36c0a1158b506660be34493c6846b09c/regen-check-alert-cli.png)

Or use your browser if you know how to setup a webserver on your workstation / pi:
[http://rain-hue.localhost](http://rain-hue.localhost)

### show history
cd to your project and run:
```
php history.php
```
![regen-history-cli](https://gitlab.com/steefdw/rain-hue/uploads/107fa78b514e52e0aac543cb1afbf67c/regen-history-cli.png)

Or use your browser if you know how to setup a webserver on your workstation / pi:
[http://rain-hue.localhost/history](http://rain-hue.localhost/history)


### Debugging arguments:

`fake`: 
don't request current data from Buienradar, but use spoofed rain data
```
  php index.php fake
```

`no-lights`: don't actually turn the Hue lights on/off while debugging
```
  php index.php no-lights
```

`fake no-lights`: use them both while debugging:
```
  php index.php fake no-lights
```
Or use your browser if you know how to setup a webserver on your workstation / pi:
[http://rain-hue.localhost/?fake&no-lights](http://rain-hue.localhost/?fake&no-lights)


## Todo's
- [ ] Add a simple write-up how to install and use this
- [ ] Cleanup code
- [ ] Make (unit) tests. The POC was blink driven development, instead of TDD 
- [x] Add a nice console rain timetable for debugging and feeling good while coding (see commit 872cb15a8a584e908bc9d46d025de9efe17c1c67)
- [ ] Make usable documentation, so other people can RTFM to make this application 
to actually work for them.
- [ ] Show how easy it is to install this on a Pi
- [ ] Contact "buienradar" and agree on a "user agent" string they feel comfortable with 
(spoofing your user agent when not testing is NOT cool)
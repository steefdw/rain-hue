<?php
/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 *
 * Date: 10-2-18
 * Time: 16:15
 */

namespace Rain;

/**
 * @property \Rain\Forecast $forecaster
 * @property string $datetime
 * @property string $loadedDatetime
 * @property array $logFiles
 */
class History {

    private $datetime = '';
    private $forecaster;
    private $loadedDatetime;
    private $logFiles;

    public function __construct($datetime = false)
    {
        $this->forecaster = new Forecast();
        $this->setDatetime($datetime);
    }

    public function sparklines()
    {
        $sparklines = [];
        foreach($this->getLogFiles() as $datetime => $data)
        {
            $sparklines[$datetime] = $this->forecaster->parseData($data)->sparkLine();
        }

        return $sparklines;
    }

    public function alerts()
    {
        $alerts = [];
        foreach($this->getLogFiles() as $datetime => $data)
        {
            $alerts[$datetime] = $this->forecaster->parseData($data)->alert();
        }

        return $alerts;
    }

    private function getLogFiles()
    {
        if($this->loadedDatetime === $this->datetime)
        {
            return $this->logFiles;
        }

        $this->loadedDatetime = $this->datetime;
        $this->logFiles       = [];

        if($files = $this->getLogFilesList())
        {
            foreach($files as $file)
            {
                if($data = file_get_contents($file))
                {
                    $datetime = $this->makeDateTimeStringFromFileName($file);

                    $this->logFiles[$datetime] = $data;
                }
            }
        }

        return $this->logFiles;
    }

    private function makeDateTimeStringFromFileName($file)
    {
        $datePart = str_replace([LOG_DIR . '/buienradar.', '.txt'], '', $file);

        $date = \DateTime::createFromFormat('Ymd-His', $datePart);

        return $date->format('Y-m-d H:i');
    }

    /**
     * @return string
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    public function setDatetime($datetime)
    {
        $this->datetime = $datetime
            ? preg_replace('/[^\-0-9]/', '', $datetime)
            : '';
    }

    public function loadFile()
    {
        if($files = $this->getLogFilesList())
        {
            foreach($files as $file)
            {
                if($data = file_get_contents($file))
                {
                    return $data;
                }
            }
        }

        return '';
    }

    private function getLogFilesList()
    {
        return glob(LOG_DIR . "/buienradar.{$this->datetime}*");
    }

}
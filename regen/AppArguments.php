<?php
/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 *
 * Date: 10-2-18
 * Time: 12:30
 */

namespace Rain;

/**
 * @property string $date
 * @property bool $fake
 * @property bool $useLights
 * @property string $longitude
 * @property string $latitude
 */
class AppArguments {

    public $date;
    public $fake;
    public $useLights;
    public $longitude;
    public $latitude;

    public function __construct($arguments, $lon, $lat)
    {
        $this->date      = isset($arguments['date'])
            ? preg_replace('/[^\-0-9]/', '', $arguments['date'])
            : date('Ymd');

        $this->fake      = isset($arguments['fake']);
        $this->useLights = !isset($arguments['no-lights']);
        $this->longitude = $lon;
        $this->latitude  = $lat;
    }
}
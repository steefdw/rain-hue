<?php

/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 */
namespace Rain;

class Cache {

    public static function get()
    {
        $files = glob(CACHE_DIR . '/buienradar.*');
        if(!$files)
        {
            return null;
        }

        $cache = self::isExpired(current($files))
               ? null
               : file_get_contents(current($files));

        foreach($files as $file)
        {
            if(self::isExpired($file) && file_exists($file))
            {
                unlink($file);
            }
        }

        return $cache;
    }

    /**
     * Checks whether the cachefile is expired or not.
     * It checks the last part of the filename that is a unixtime stamp<br>
     * e.g. whatever.<b>1461505021</b>
     *
     * @param string $filename
     * @return bool
     */
    private static function isExpired($filename)
    {
        $time = substr(strrchr($filename, '.'), 1);

        return ($time < time());
    }

    public static function set($value)
    {
        // self::delete(); // files should have been deleted in the self::get(), so why do it again?

        $file   = CACHE_DIR . '/buienradar.' . (time() + CACHE_TIME);
        $handle = fopen($file, 'wb');

        fwrite($handle, $value);
        fclose($handle);
    }

    public static function log($value)
    {
        $file   = LOG_DIR . '/buienradar.' . date('Ymd-His').'.txt';
        $handle = fopen($file, 'wb');

        fwrite($handle, $value);
        fclose($handle);
    }

    public static function delete()
    {
        $files = glob(CACHE_DIR . '/buienradar.*', GLOB_BRACE);

        if($files)
        {
            foreach($files as $file)
            {
                if(file_exists($file))
                {
                    unlink($file);
                }
            }
        }
    }

    public static function read($key)
    {
        $key    = preg_replace('/[^a-z]/i', '', $key);
        $file   = CACHE_DIR . '/'.$key;

        return file_get_contents($file);
    }

    public static function write($key, $value)
    {
        $key    = preg_replace('/[^a-z]/i', '', $key);
        $file   = CACHE_DIR . '/'.$key;
        $handle = fopen($file, 'wb');

        fwrite($handle, $value);
        fclose($handle);
    }

}

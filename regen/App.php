<?php
/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 *
 * Date: 17-1-18
 * Time: 14:43
 */

namespace Rain;

/**
 * @property array $config
 * @property \Rain\AppArguments $arguments
 */
class App {

    public $arguments;
    private $config;

    public function __construct($argv) {
        define('ROOT_DIR', dirname(__DIR__));
        define('CACHE_DIR', ROOT_DIR.'/cache');
        define('CACHE_TIME', 4*60);
        define('LOG_DIR', ROOT_DIR.'/log');

        $this->config = $this->getSettings();
        $this->arguments = $this->parseArguments($argv);
    }

    private function parseArguments($argv)
    {
        if($argv)
        {
            parse_str(implode('&', $argv), $arguments);
        }
        else
        {
            $arguments = $_GET;
        }

        define('RUNNING_IN_CONSOLE', (bool)$argv);

        return new AppArguments(
            $arguments,
            $this->getConfig('longitude'),
            $this->getConfig('latitude')
        );
    }

    public function welcomeMessage()
    {
        return
            Output::rain('---------------------------------------').PHP_EOL.
            Output::rain(' Regenvoorspelling '.date('Y-m-d H:i:s').' ').PHP_EOL.
            Output::rain('---------------------------------------').PHP_EOL
        ;
    }

    public function getConfig($key, $default = null)
    {
        if(isset($this->config[$key]))
        {
            return $this->config[ $key ];
        }

        return $default;
    }

    private function getSettings()
    {
        if(file_exists(ROOT_DIR . '/config.php'))
        {
            return (array)include ROOT_DIR . '/config.php';
        }

        return [];
    }
}
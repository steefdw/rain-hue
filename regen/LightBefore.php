<?php
/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 *
 * Date: 24-1-18
 * Time: 17:22
 */

namespace Rain;
use Phue;

/**
 * A value object that stores the state of the light before we did something with it
 *
 * @property \Rain\App $app
 * @property bool $on
 * @property array $rgb
 * @property int $brightness
 * @property Phue\Light $light
 */
class LightBefore {

    public function __construct(Phue\Light $light)
    {
        $this->on = $light->isOn();
        $this->rgb = $light->getRGB();
        $this->brightness = $light->getBrightness();
        $this->light = $light;
    }

    public function backToInitialState()
    {
        $this->light->setAlert('none');

        if(isset($this->rgb['red']))
        {
            $this->light->setRGB(
                $this->validate($this->rgb['red']),
                $this->validate($this->rgb['green']),
                $this->validate($this->rgb['blue'])
            );
        }

        if(empty($this->on))
        {
            $this->light->setOn(false);
        }
        else
        {
            $this->light->setBrightness($this->validate($this->brightness));
        }
    }

    /**
     * @param float|int $value
     * @return int
     */
    private function validate($value)
    {
        if($value >= 0 && $value <= 255)
        {
            return (int)$value;
        }

        if($value < 0)
        {
            return 0;
        }

        return 255;
    }
}
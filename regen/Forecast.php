<?php

/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 */
namespace Rain;

class Forecast {

    private $forecasts = [];
    public $types = [
        0 => [
            'name' => 'droog',
            'ft'   => 'wordt het droog',
            'verb' => 'droog'
        ],
        1 => [
            'name' => 'lichte motregen',
            'ft'   => 'gaat het licht motregenen',
            'verb' => 'licht motregenen'
        ],
        2 => [
            'name' => 'motregen',
            'ft'   => 'gaat het motregenen',
            'verb' => 'motregenen'
        ],
        3 => [
            'name' => 'lichte regen',
            'ft'   => 'gaat het licht regenen',
            'verb' => 'licht regenen'
        ],
        4 => [
            'name' => 'regen',
            'ft'   => 'gaat het regenen',
            'verb' => 'regenen'
        ],
        5 => [
            'name' => 'stevige bui',
            'ft'   => 'begint het stevig te buien',
            'verb' => 'stevig buien'
        ],
        6 => [
            'name' => 'zeer zware bui',
            'ft'   => 'begint het zeer zwaar te buien',
            'verb' => 'zeer zwaar buien'
        ],
        7 => [
            'name' => 'wolkbreuk',
            'ft'   => 'komt er een wolkbreuk',
            'verb' => 'wolkbreuken'
        ],
        8 => [
            'name' => 'zeer zware wolkbreuk',
            'ft'   => 'komt er een zeer zware wolkbreuk',
            'verb' => 'zeer zwaar wolkbreuken'
        ]
    ];

    public static function rainRound($forecast)
    {
        $rain_exact = pow(10, (($forecast - 109) / 32)); // mm/per uur = 10^((waarde -109)/32)

        return round($rain_exact, 3);
    }

    public static function rainType($rain_round)
    {
        if($rain_round < 0.18)
            $rain_type = 0;
        elseif($rain_round < 0.6)
            $rain_type = 1;
        elseif($rain_round < 0.9)
            $rain_type = 2;
        elseif($rain_round < 1.5)
            $rain_type = 3;
        elseif($rain_round < 8)
            $rain_type = 4;
        elseif($rain_round < 18)
            $rain_type = 5;
        elseif($rain_round < 25)
            $rain_type = 6;
        elseif($rain_round < 50)
            $rain_type = 7;
        else
            $rain_type = 8;

        return $rain_type;
    }

    public function typeName($rain_type)
    {
        return $this->types[$rain_type]['name'];
    }

    public function typeVerb($rain_type)
    {
        return $this->types[$rain_type]['verb'];
    }

    public function forecastText($rain_type)
    {
        return $this->types[$rain_type]['ft'];
    }

    public function parseData($data)
    {
        $forecasts = array();
        $lines = explode("\n", $data);

        foreach($lines as $key => $line)
        {
            $forecast = explode("|", $line);

            if($key > 23 OR count($forecast) !== 2)
                continue;

            list($mmph, $time) = $forecast;

            $height = ($mmph * $mmph) / 100;
            if($height > 300)
                $height = 300;

            if(str_replace(':', '', $time) > 2354)
                $time_is_next_day = true; // if the entry is the last one of this day

            if(isset($time_is_next_day))
                $time_diff = round(((strtotime($time) + 86400) - time()) / 60, 0);
            else
                $time_diff = round((strtotime($time) - time()) / 60, 0);

            $rain_round = $this->rainRound($mmph);
            $rain_type  = $this->rainType($rain_round);

            $forecasts[$key] = (object)array(
                'time_diff'  => $time_diff,
                'height'     => $height,
                'time'       => $time,
                'rain_type'  => $rain_type,
                'rain_round' => $rain_round,
            );
        }

        $this->forecasts = $forecasts;

        return $this;
    }

    public function messages()
    {
        $time_list = [];
        $current = 0;

        foreach($this->forecasts as $key => $forecast)
        {
            if($forecast->time_diff > 0)
            {
                if($current != $forecast->rain_type)
                {
                    $minutes = str_pad($forecast->time_diff, 3, " ", STR_PAD_LEFT);
                    $time_list[] = 'over ' . $minutes . ' minuten ' . $this->forecastText($forecast->rain_type);
                    $current = $forecast->rain_type;
                }
            }
            elseif($forecast->time_diff > -5)
            {
                $current = $forecast->rain_type;
            }
        }

        return (count($time_list))
            ? $time_list
            : ['het blijft de aankomende 2 uur droog'];
    }

    public function sparkLine()
    {
        $output = '';
        $blocks = [
            "_",
            "▁",
            "▂",
            "▃",
            "▄",
            "▅",
            "▆",
            "▇",
            "█",
        ];

        foreach($this->forecasts as $forecast)
        {
            if($forecast->rain_type < 1)
                $color = 'light green';
            elseif($forecast->rain_type <= 2)
                $color = 'green';
            elseif($forecast->rain_type == 3)
                $color = 'light blue';
            elseif($forecast->rain_type == 5)
                $color = 'light red';
            elseif($forecast->rain_type > 5)
                $color = 'red';
            else
                $color = 'blue';

            $output .= Output::text($blocks[$forecast->rain_type], $color);
        }

        return $output;
    }

    public function alert()
    {
        $minutes  = 30;
        $length   = round($minutes / 5);
        $watching = array_slice($this->forecasts, 1, $length);
        $now      = current($this->forecasts);
        $lastAlert = Cache::read('lastAlert');

        $type = '';
        foreach($watching as $i => $forecast)
        {
            if(($now->rain_type >= 2 || $lastAlert === 'rain') && $forecast->rain_type <= 2)
                $type = 'sun';

            if($forecast->rain_round > 1.0 || $forecast->rain_type > 2)
            {
                Cache::write('lastAlert', 'rain');

                return 'rain';
            }
        }

        Cache::write('lastAlert', $type);

        return $type;
    }

}
<?php

/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 */

namespace Rain;

class Rain {

    public static function fetch($arguments)
    {
        if($arguments->fake)
        {
            $history = new History($arguments->date);

            $msg = 'fake data loaded';
            if($date = $history->getDatetime())
            {
                echo Output::info($msg.' from '.$date);

                return $history->loadFile();
            }

            echo Output::info('random '. $msg);

            return self::fakeData();
        }

        if(!$content = Cache::get())
        {
            $data = Curl::buienradar($arguments->longitude, $arguments->latitude);
            if(!empty($data->content) && !$data->header['error_message'])
            {
                $content = $data->content;
                Cache::set($content);
                Cache::log($content);
            }
            else
            {
                if(empty($data->content))
                {
                    echo Output::warn('ERROR: "No content received from buienradar"');
                }

                echo Output::warn('ERROR-MESSAGE: "'.$data->header['error_message'].'"');
                echo Output::warn('ERROR-NUMBER: "'.$data->header['error_number'].'"');
            }
        }

        return $content;
    }

    public static function fakeData()
    {
        $data = '';
        $rain = 0;
        $hour = date('H');
        $min = date('i')-4;
        foreach(range(0,23) as $tick)
        {
            $data .= str_pad($rain, 3, '0', STR_PAD_LEFT);
            $data .= '|'.$hour.':'.str_pad($min, 2, '0').PHP_EOL;

            $rain += 7.5;
            $min += 5;
            if($min >= 60)
            {
                $min -= 60;
                $hour++;

                if($hour > 23)
                {
                    $hour = '00';
                }
            }
        }

        return $data;
    }
}

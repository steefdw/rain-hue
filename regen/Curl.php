<?php
/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 */
namespace Rain;

class Curl {

    /**
     * Get a web file from a URL.
     *
     * @param string $url
     * @return object The response
     */
    private static function getUrl( $url )
    {
        $user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.109 Safari/537.36';
        $options = [
            CURLOPT_RETURNTRANSFER => true,  // return web page
            CURLOPT_HEADER         => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => true,  // follow redirects
            CURLOPT_ENCODING       => '',    // handle all encodings
            CURLOPT_USERAGENT      => $user_agent, // who am i
            CURLOPT_AUTOREFERER    => true,  // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 10,    // timeout on connect
            CURLOPT_TIMEOUT        => 15,    // timeout on response
            CURLOPT_MAXREDIRS      => 5,     // stop after 5 redirects
            CURLOPT_FAILONERROR    => true,  // This ensures 404 Not Found (and similar) will be treated as errors
            CURLOPT_SSL_VERIFYPEER => false, // Disabled SSL Cert checks
            CURLOPT_SSL_VERIFYHOST => 2,     // Check that the common name exists and that it matches the host name of the server
        ];

        $curl = curl_init( $url );
        curl_setopt_array( $curl, $options );

        $content   = curl_exec( $curl );
        $error     = curl_errno( $curl );
        $error_msg = curl_error( $curl );
        $header    = curl_getinfo( $curl );
        curl_close( $curl );

        $header['error_number']  = $error;
        $header['error_message'] = $error_msg;

        return (object)[
            'header'  => $header,
            'content' => $content,
        ];
    }

    /**
     * Get contents from Buienradar
     *
     * @param $longitude
     * @param $latitude
     * @return object
     */
    public static function buienradar($longitude, $latitude)
    {
        $url = 'https://gps.buienradar.nl/getrr.php?lat='.$latitude.'&lon='.$longitude;
        echo Output::success('Fetching '.$url);

		return self::getUrl($url);

		// before 20 juli 2017 update:
		// see: https://www.buienradar.nl/overbuienradar/gratis-weerdata
        // return self::getUrl('https://gpsgadget.buienradar.nl/data/raintext/?lat='.$latitude.'&lon='.$longitude);
    }

}

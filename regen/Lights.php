<?php
/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 * Date: 17-1-18
 * Time: 16:32
 */

namespace Rain;
use Phue;

/**
 * @property \Rain\App $app
 * @property LightBefore[] $old
 * @property Phue\Light[] $lights
 */
class Lights {

    private $old = [];

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->lights = $this->getActiveLights();
    }

    /**
     * @return Phue\Light[]
     */
    private function getActiveLights()
    {
        try {
            $client = new Phue\Client($this->app->getConfig('host'), $this->app->getConfig('username'));
            $lights = $client->getLights();
        }
        catch (Phue\Transport\Exception\ConnectionException $e) {
            echo Output::warn('There was a problem accessing the bridge');
            exit;
        }

        foreach($lights as $lightId => $light)
        {
            if($this->useLight($light) === false) {
                unset($lights[$lightId]);
                continue;
            }

            $this->old[$lightId] = new LightBefore($light);
        }

        if(!count($this->old))
        {
            echo Output::warn('no active lights?');
            die;
        }

        return $lights;
    }

    public function loop($alert)
    {
        if(!$alert)
        {
            die;
        } // ('nothing to do')

        foreach($this->lights as $lightId => $light)
        {
            $this->lightStateMessage($lightId, $light, $alert);

            if($this->app->arguments->useLights)
            {
                $this->activateLight($light, $alert);
            }
        }

        sleep(5.3);

        if($this->app->arguments->useLights)
        {
            $this->deActivateLights();
        }
    }

    /**
     * Activate a light based on the rain message
     * @param \Phue\Light $light
     * @param string $weatherType
     */
    private function activateLight(Phue\Light $light, $weatherType)
    {
        if(!$light->isOn())
        {
            $light->setOn();
        }

        if($weatherType === 'sun')
        {
            $light->setRGB(250, 255, 82); // sunny
        }
        else
        {
            $light->setRGB(67, 83, 226); // rain
        }

        $light->setBrightness(255);
        $light->setAlert('lselect'); // select, lselect, none
    }

    /**
     * Everything back to normal
     */
    private function deActivateLights()
    {
        foreach ($this->old as $oldLight)
        {
            $oldLight->backToInitialState();
        }
    }

    private function useLight(Phue\Light $light)
    {
        $light_name = $light->getName();
        $used_lights = $this->app->getConfig('used_lights', []);

        return in_array($light_name, $used_lights, true);
    }

    /**
     * @param $lightId
     * @param Phue\Light $light
     * @param string $weatherType
     */
    private function lightStateMessage($lightId, $light, $weatherType)
    {
        $weatherTypeMessage = ($weatherType === 'sun')
            ? Output::sun('naar zonnig')
            : Output::rain('naar regen');

        echo "Id #{$lightId}"
            . ' stond ' . ($light->isOn() ? 'aan' : 'uit')
            . ' - rgb(' . implode(',', $light->getRGB()) . ')'
            . ' - bri:' . $light->getBrightness()
            . " - {$weatherTypeMessage}"
            . " ({$light->getName()})"
        . PHP_EOL;
    }
}
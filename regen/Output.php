<?php
/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 *
 * Date: 10-2-18
 * Time: 12:23
 */


namespace Rain;
use \Clicoh\Alert;

/**
 * Colorize the output in the terminal
 */
class Output extends \Clicoh\Output {

    /**
     * @param string $message
     * @return string
     */
    public static function warn($message)
    {
        return Alert::warn($message);
    }

    /**
     * @param string $message
     * @return string
     */
    public static function success($message)
    {
        return Alert::success($message);
    }

    /**
     * @param string $message
     * @return string
     */
    public static function info($message)
    {
        return Alert::info($message);
    }

    /**
     * @param string $message
     * @return string
     */
    public static function rain($message)
    {
        return self::set(
            $message,
            'white',
            'blue'
        );
    }

    /**
     * @param string $message
     * @return string
     */
    public static function sun($message)
    {
        return self::set(
            $message,
            'black',
            'yellow'
        );
    }

}

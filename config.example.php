<?php

return [
    'host'          => '192.168.x.x', // Go to http://www.meethue.com/api/nupnp to find the LAN IP of your hue bridge
    'username'      => '',
    'latitude'      => '',
    'longitude'     => '',
    'used_lights' => [
        'livingroom 1',
        'livingroom 2',
    ],
];
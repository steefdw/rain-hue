<?php
/**
 * Rain Hue
 *
 * @see: https://gitlab.com/steefdw/rain-hue
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/rain-hue/blob/master/LICENCE
 *
 * Date: 10-2-18
 * Time: 15:37
 *
 * Usage:
 *
 * In the browser:
 * http://rain-hue.localhost/history.php?date=20180201 for seeing the sparklines of a day
 * http://rain-hue.localhost/history.php?date=20180201-14 for seeing sparklines of an hour
 * http://rain-hue.localhost/history.php?date=20180201-0415 for seeing the sparkline of a specific time
 *
 * In the terminal:
 * $ php history.php date=20180202 for seeing sparklines of a day
 * $ php history.php date=20180202-04 for seeing sparklines of an hour
 * $ php history.php date=20180202-0415 for seeing the sparkline of a specific time
 *
 * Example: $ php history.php date=20180202-04
 * will output something like:
    2018-02-02 02:00 ▃▂___▁▁▂▁▁__▁___________ sun
    2018-02-02 02:05 ▁___▁▁▃▁________________ rain
    2018-02-02 02:10 ▁___▁▁▃▁________________ rain
    2018-02-02 02:15 _▁▂▃▂▁_▁▂_______________ rain
    2018-02-02 02:20 ▂▂▃▂__▂▁________________ rain
    2018-02-02 02:25 ▃▃▁__▃▁_________________ rain
    2018-02-02 02:30 ▃▁_▁▃▁__________________ rain
    2018-02-02 02:35 ▃▁_▁▃▁__________________ rain
    2018-02-02 02:40 __▂▂____▁_______________ sun
    2018-02-02 02:45 _▃▁____▁________________ rain
    2018-02-02 02:50 ▁_______________________ sun
    2018-02-02 02:55 ________________________
 */

if(!file_exists(__DIR__ . '/vendor/autoload.php'))
{
    die("\033[31mPlease run 'composer install' first\033[0m".PHP_EOL);
}
require_once __DIR__ . '/vendor/autoload.php';

$app = new \Rain\App($argv);
if(RUNNING_IN_CONSOLE === false)
{
    echo '<pre>';
}

$history = new \Rain\History($app->arguments->date);

$sparklines = $history->sparklines();
$alerts     = $history->alerts();
foreach($sparklines as $datetime => $sparkline)
{
    $alert = '';
    if(isset($alerts[$datetime]))
    {
        $alert = $alerts[ $datetime ];
    }

    echo $datetime.' '.$sparkline.' '.$alert.PHP_EOL;
}
